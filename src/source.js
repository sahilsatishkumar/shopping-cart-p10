import React from 'react'
import { Button, Icon } from 'antd'

const COLORS = {
  MAROON: {
    "name": "maroon",
    "hexcode": "#800000"
  },
  NAVY: {
    "name": "navy",
    "hexcode": "#000080"
  },
  WHITE: {
    "name": "white",
    "hexcode": "#FFFFFF"
  },
  BLACK: {
    "name": "black",
    "hexcode": "#000000"
  },
  GREY: {
    "name": "grey",
    "hexcode": "#808080"
  },
  YELLOW: {
    "name": "yellow",
    "hexcode": "#FFFF00"
  },
  ROYAL_BLUE: {
    "name": "royal-blue",
    "hexcode": "#4169E1"
  },
  PURPLE: {
    "name": "purple",
    "hexcode": "#800080"
  }
}

const productList = [
  {
    "id": "1",
    "image": (color) => `https://cdn.shopify.com/s/files/1/1650/5551/products/${color}-fold_1080x.jpg`,
    "name": "Crew neck Tshirt",
    "variation": "solid green",
    "style": "ms13kt1906",
    "selected_color": COLORS.MAROON,
    "selected_size": {
      "name": "small",
      "code": "s"
    },
    "available_options": {
      "colors": [
        COLORS.MAROON,
        COLORS.NAVY,
        COLORS.WHITE,
        COLORS.BLACK,
        COLORS.GREY,
        COLORS.YELLOW,
        COLORS.ROYAL_BLUE,
        COLORS.PURPLE,
      ],
      "sizes": [
        {
          "name": "small",
          "code": "s"
        },
        {
          "name": "medium",
          "code": "m"
        },
        {
          "name": "large",
          "code": "l"
        },
        {
          "name": "extra large",
          "code": "xl"
        }
      ]
    },
    "quantity": 1,
    "originalprice": 11,
    "price": 11,
    "currency": "$"
  },
  {
    "id": "2",
    "name": "V neck tshirt",
    "image": (color) => `https://cdn.shopify.com/s/files/1/1650/5551/products/${color}_zoom_1024x1024.jpg`,
    "variation": "pink rainbow",
    "style": "ms13kt1906",
    "selected_color": COLORS.BLACK,
    "selected_size": {
      "name": "small",
      "code": "s"
    },
    "available_options": {
      "colors": [
        COLORS.BLACK,
        COLORS.WHITE,
      ],
      "sizes": [
        {
          "name": "small",
          "code": "s"
        },
        {
          "name": "medium",
          "code": "m"
        },
        {
          "name": "large",
          "code": "l"
        },
        {
          "name": "extra large",
          "code": "xl"
        }
      ]
    },
    "quantity": 1,
    "originalprice": 17,
    "price": 17,
    "currency": "$"
  },
  {
    "id": "3",
    "name": "Full Sleeve T-Shirt",
    "image": "https://cdn.shopify.com/s/files/1/1650/5551/products/front_fc151c40-5699-41d3-a9f7-7ea27cef9097_1080x.jpg",
    "variation": "yellow",
    "style": "ms13kt1906",
    "selected_color": COLORS.YELLOW,
    "selected_size": {
      "name": "small",
      "code": "s"
    },
    "available_options": {
      "colors": [
        COLORS.YELLOW
      ],
      "sizes": [
        {
          "name": "small",
          "code": "s"
        },
        {
          "name": "medium",
          "code": "m"
        },
        {
          "name": "large",
          "code": "l"
        },
        {
          "name": "extra large",
          "code": "xl"
        }
      ]
    },
    "quantity": 1,
    "originalprice": 21,
    "price": 9,
    "currency": "$"
  },
  {
    "id": "4",
    "name": "Black Raglan",
    "image": "https://cdn.shopify.com/s/files/1/1650/5551/products/front_ad270ba3-aa2e-46fa-8601-2352d466e798_1080x.jpg",
    "variation": "mens red",
    "style": "ms13kt1906",
    "selected_color": COLORS.BLACK,
    "selected_size": {
      "name": "medium",
      "code": "m"
    },
    "available_options": {
      "colors": [
        COLORS.BLACK
      ],
      "sizes": [
        {
          "name": "small",
          "code": "s"
        },
        {
          "name": "medium",
          "code": "m"
        },
        {
          "name": "large",
          "code": "l"
        },
        {
          "name": "extra large",
          "code": "xl"
        }
      ]
    },
    "quantity": 1,
    "originalprice": 22,
    "price": 22,
    "currency": "$"
  }
]

const columns = productListLength => [{
  title: '',
  dataIndex: 'image',
  key: 'image',
  align: 'center'
}, {
  title: `${productListLength} items`,
  dataIndex: 'productDescription',
  key: 'productDescription',
}, {
  title: 'Size',
  dataIndex: 'size',
  key: 'size',
  align: 'center'
}, {
  title: 'Quantity',
  dataIndex: 'quantity',
  key: 'quantity',
  align: 'center'
}, {
  title: 'Price',
  dataIndex: 'price',
  key: 'price',
  align: 'center'
}];

const IconText = ({ type, text , onClick}) => (
  <Button dashed="true" onClick = {() =>onClick && onClick()}>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text}
  </Button>
);

export {
  productList,
  columns,
  IconText
}