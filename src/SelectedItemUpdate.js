import React, { Component } from 'react'
import { Col, Icon, Row, Card, Radio, Menu, Dropdown } from 'antd'

import {IconText} from './source'

class SelectedItemUpdate extends Component {
  constructor(props){
    super(props)
    this.state = {
      ...props.selectedItem
    }
  }

  updateSelectedItem = (keyName, value) => this.setState({[keyName]: value})

  render(){
    const product = this.state
    return (
      <Row>
        <Col span={12}>
          <Card 
            title={product.name}
            actions={[<IconText type="edit" text="Edit" onClick={_ => this.props.updateCartItem(product.id, null, {...this.state})}/>]}
          >
            <p>Style: {product.style}</p>
            
            <Radio.Group onChange={e => this.updateSelectedItem('selected_color', JSON.parse(e.target.value))} defaultValue={JSON.stringify(product.selected_color)}>
              {product.available_options.colors.map(color => (
                <Radio.Button key={color.hexcode} value={JSON.stringify(color)} style={{backgroundColor: color.hexcode}}/>
              ))}
            </Radio.Group>
            <p>Color: <b>{product.selected_color.name.toUpperCase()}</b></p>

            <Dropdown 
              overlay={(
                <Menu>
                  {product.available_options.sizes.map(size => <Menu.Item onClick={_ => this.updateSelectedItem('selected_size', size)} key={size.code}>{size.name}</Menu.Item>)}
                </Menu>
              )}>
              <span>
                Size: {product.selected_size.name}
                <Icon type="caret-down" />
              </span>
            </Dropdown> <br/>

            {/* Quantity: <InputNumber size="large" min={1} max={99} defaultValue={product.quantity} onChange={val => this.updateSelectedItem('quantity', val)}/> */}

          </Card>
        </Col>
        <Col span={1}/>
        <Col span={11}>
          <img width={'90%'} alt={product.name} src={typeof product.image === 'function' ? product.image(product.selected_color.name): product.image}/>
        </Col>
      </Row>
      
    )
  }
}

export default SelectedItemUpdate