import React, { Component } from 'react'
import { Col } from 'antd'

const computeTotal = (productList) => {
  return productList.reduce( (acc, currItem) => {
    const total = currItem.price * currItem.quantity
    const originalTotal = currItem.originalprice * currItem.quantity
    const savings = originalTotal - total
    return {savings: acc.savings + savings, total: acc.total + total}
  }, {savings: 0, total: 0})
}

class CartTotal extends Component {
  render() {
    const {total, savings} = computeTotal(this.props.productList)
    return (
      <React.Fragment>
        <Col span={12} offset={12}>
          <p> LIST PRICE: <s> $ {total + savings}</s></p>
          <h3> ESTIMATED TOTAL: $ {total}</h3>
        </Col>
      </React.Fragment>
    )
  }
}

export default CartTotal