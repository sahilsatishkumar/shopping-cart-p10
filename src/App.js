import React, { Component } from 'react'
import { Col, Table, InputNumber, Modal, Row } from 'antd'

import SelectedItemUpdate from './SelectedItemUpdate'
import CartTotal from './CartTotal'
import {productList, columns, IconText} from './source'

class App extends Component {
  state = {
    selectedItem: null,
    productList
  }

  renderProductRow = product => ({
    key: product.id,
    image: (
      <img 
        height={128} 
        src={typeof product.image === 'function' ? product.image(product.selected_color.name): product.image}
        alt={product.name}
      />
    ),
    productDescription: (
      <React.Fragment>
        <b>{product.name}</b><br/>
        {product.style}<br/>
        {product.selected_color.name}<br/>
        <IconText type="edit" text="Edit" onClick = {() => this.showModal(product.id)}/>&emsp;
        <IconText type="delete" text="Remove" />&emsp;
        <IconText type="save" text="Save For later" />&emsp;
      </React.Fragment>
    ),
    size: (<b>{product.selected_size.code.toUpperCase()}</b>),
    quantity: (
      <InputNumber size="large" min={1} max={99} defaultValue={product.quantity} onChange={val => this.updateCartItem(product.id, 'quantity', val)}/>
    ),
    price: (
      <span>
        <s>{product.currency}{product.originalprice}</s><br/>
        <b>{product.currency}{product.price}</b>
      </span>
    )
  })

  updateCartItem = (id, keyName, newVal) => {
    let productList = this.state.productList.map(product => 
      (id === product.id ? 
        (Boolean(keyName) ? 
          ({...product, [keyName]: newVal}) : 
          ({...newVal})) : 
        product))
    this.setState({productList, selectedItem: null})
  }

  showModal = (selectedItem) => this.setState({selectedItem})

  getSelectedItemModal = (selectedItem) => (
    <Modal
      visible={Boolean(selectedItem)}
      onOk={this.handleOk}
      onCancel={_ => this.showModal(null)}
      width={'80%'}
      footer={null}
    >
      {selectedItem && <SelectedItemUpdate selectedItem={selectedItem} updateCartItem={this.updateCartItem}/>}
    </Modal>
  )

  render() {
    return (
      <React.Fragment>
        <Row>
          <Col span={2}>
            {this.getSelectedItemModal((
              Boolean(this.state.selectedItem) ? 
                this.state.productList[this.state.selectedItem - 1] : 
                false))}
          </Col>
          <Col span={20}><br/>
            <h1>Your Shopping cart</h1>
            <Table
              dataSource={this.state.productList.map(product => this.renderProductRow(product))}
              columns={columns(this.state.productList.length)}
              pagination={false}
            />
          </Col>
          <Col span={2}/>
        </Row>
        <Row>
          <CartTotal productList={this.state.productList}/>
        </Row>
      </React.Fragment>
    );
  }
}

export default App;
